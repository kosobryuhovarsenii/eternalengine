// Подключение сладйеров
$("document").ready(function () {
  // Сладйеры на главной
  $(".for-slider").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: $(".for-slider__left"),
    nextArrow: $(".for-slider__right"),
  });

  $(".reviews-slider").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: $(".reviews-slider__left"),
    nextArrow: $(".reviews-slider__right"),
  });
});
