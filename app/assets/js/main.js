$(document).ready(function () {
    // Бургер-меню
    const menuBtn = $(".header-button__menu");
    const menu = $('.header-block__menu');

    menuBtn.on("click", function () {
        if (!menu.hasClass("active")) {
            menu.addClass("active");
            menuBtn.addClass("active");
        } else {
            menu.removeClass("active");
            menuBtn.removeClass("active");
        }
    });

    //Табы
    const resultBtn = $('.result-buttons').find('h3');
    const resultBlock = $('.result-block');

    resultBtn.on('click', function () {
        resultBtn.removeClass('active');
        resultBlock.removeClass('active');
        setTimeout(function () {
            resultBlock.css('display', 'none')
        }, 400);

        let btn = $(this);
        let btnName = btn.attr('data-name');

        resultBlock.each(function () {
            let block = $(this);
            let blockName = block.attr('data-name');

            if (btnName === blockName) {
                btn.addClass('active');
                setTimeout(function () {
                    block.css('display', 'block')
                }, 400);
                block.addClass('active');
            }

        });
    })

    //faq list
    const faqBtn = $('.faq-block__item-head');
    const faqBody = $('.faq-block__item-body');

    faqBtn.on('click', function () {
        faqBody.not($(this).next()).slideUp();
        $(this).toggleClass('active').next().slideToggle(300);
        faqBtn.not($(this)).removeClass('active');
    })

    //modal
    const modalBtn = $('.header-button');

    modalBtn.on('click', function (e) {
        e.preventDefault();
        $('.modal-bg').addClass('active');
        $('.modal').addClass('active');
    })
});
